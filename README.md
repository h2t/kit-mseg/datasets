The MSeg Motion Segmentation Evaluation Framework
===

**MSeg** is a framework for the [ArmarX](https://armarx.humanoids.kit.edu/index.html) robot development environment,
built to run and evaluate motion segmentation algorithms written in C++, Java, Python or MATLAB. Please refer to the
[MSeg documentation](https://mseg.readthedocs.io/) for guides on how to install the framework and how to get started.

If you encounter a problem, feel free to open a new issue in the
[issue tracker](https://gitlab.com/h2t/kit-mseg/mseg/issues) or send an email to
<a href="mailto:incoming+h2t/kit-mseg/mseg@gitlab.com">incoming+h2t/kit-mseg/mseg@gitlab.com</a>
(this will create an issue in the issue tracker).

--- *The rest of this document is relevant for developers only* ---


# MSeg Datasets

This repository contains labelled whole-body human motion datasets to use with MSeg. The motion recordings are
transmitted to the motion segmentation algorithms and the included ground truth is used to score the algorithms.

The motion recordings are taken from the
[KIT Whole-Body Human Motion Database](https://motion-database.humanoids.kit.edu/).


# License

Licensed under the [GPL 2.0 License](./LICENSE.md).
